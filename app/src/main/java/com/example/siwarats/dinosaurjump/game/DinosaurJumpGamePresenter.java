package com.example.siwarats.dinosaurjump.game;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.siwarats.dinosaurjump.R;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.Calendar;

@EBean
public class DinosaurJumpGamePresenter
        implements DinosaurJumpGameContract.Presenter, Obstacle.GameOverListener {

    private int JUMP_HEIGHT = 0;
    private int JUMP_DURATION = 0;
    private int GAME_SPEED = 0;

    private DinosaurJumpGameContract.View view;

    private AnimatorSet characterAnimatorSet = new AnimatorSet();

    private TimerUtil gameLoop, timeLoop;

    private boolean isGameOver = false;

    private long startTime = 0;

    @RootContext
    protected Context context;

    @Override
    public void init(final DinosaurJumpGameContract.View view) {
        this.view = view;
        Bitmap obstacle = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ic_obstacle);
        Bitmap character = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ic_obstacle);

        JUMP_HEIGHT = (int) ((obstacle.getHeight() + character.getWidth() + obstacle.getWidth()) * 1.25);
        JUMP_DURATION = (int) (JUMP_HEIGHT * 2);
        GAME_SPEED = (int) (JUMP_DURATION * 4);

        gameLoop = new TimerUtil(10000, GAME_SPEED, new TimerUtil.OnTimeChange() {
            @Override
            public void onTravel(long milli) {
//                if (new Random().nextBoolean()) {
                if (!isGameOver) {
                    createObstacle();
                }
//                }
            }

            @Override
            public void onFinish() {
                //Infinity loop
                gameLoop.start();
            }
        });
        timeLoop = new TimerUtil(10000, 25, new TimerUtil.OnTimeChange() {
            @Override
            public void onTravel(long milli) {
                if (!isGameOver) {
                    view.setTextViewScore(calculateScore() + "");
                }
            }

            @Override
            public void onFinish() {
                //Infinity loop
                timeLoop.start();
            }
        });
    }

    @Override
    public void onJumpClick() {
        if (!characterAnimatorSet.isRunning()) {
            characterAnimatorSet.start();
        }
    }

    @Override
    public void onResume() {
        setupCharacter();
        view.showStartDialog(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                startGame();
            }
        });
    }

    @Override
    public void onPause() {
        gameLoop.cancel();
//        view.showPauseDialog(new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
    }

    private void setupCharacter() {
        int fromY = (int) view.getCharacter().getY();
        int toY = fromY - JUMP_HEIGHT;
        ObjectAnimator characterTranY = ObjectAnimator
                .ofFloat(view.getCharacter(), "tranY", fromY, toY);
        ObjectAnimator rotate = ObjectAnimator
                .ofFloat(view.getCharacter(), "rotate", 0, 45);

        characterTranY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float y = (float) animation.getAnimatedValue();
                view.getCharacter().setTranslationY(y);
            }
        });
        rotate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float rotate = (float) animation.getAnimatedValue();
                if (rotate > 24.5) {
                    rotate -= (rotate - 24.5) * 2;
                }
                view.getCharacter().setRotation(-rotate);
            }
        });
        characterTranY.setDuration(JUMP_DURATION);
        characterTranY.setRepeatMode(ValueAnimator.REVERSE);
        characterTranY.setInterpolator(new LinearInterpolator());
        characterTranY.setRepeatCount(1);
//        rotate.setRepeatMode(ValueAnimator.REVERSE);
        rotate.setDuration(JUMP_DURATION);
        rotate.setInterpolator(new LinearInterpolator());
//        rotate.setRepeatCount(1);
        characterAnimatorSet.setDuration(JUMP_DURATION);
        characterAnimatorSet.playTogether(characterTranY, rotate);
    }

    private void createObstacle() {
        view.getObstacleContainer().post(new Runnable() {
            @Override
            public void run() {
                ImageView imageView = new ImageView(context);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.BOTTOM;
                imageView.setLayoutParams(layoutParams);
                imageView.setImageResource(R.drawable.ic_obstacle);

                Obstacle obstacle = new Obstacle(view.getObstacleContainer(),
                        imageView, view.getCharacter());
                obstacle.setGameOverListener(DinosaurJumpGamePresenter.this);
                obstacle.start();
            }
        });
    }

    private void startGame() {
        isGameOver = false;
        startTime = Calendar.getInstance().getTimeInMillis();
        gameLoop.start();
        timeLoop.start();
    }

    private void stopGame() {
        timeLoop.cancel();
        gameLoop.cancel();
        view.getObstacleContainer().removeAllViews();
    }

    private long calculateScore() {
        return (Calendar.getInstance().getTimeInMillis() - startTime) / 10;
    }

    @Override
    public void onGameOver(Obstacle obstacle) {
        stopGame();
        if (!isGameOver) {
            isGameOver = true;
            view.showEndGameDialog(calculateScore(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    startGame();
                }
            });
        }
    }
}
