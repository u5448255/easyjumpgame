package com.example.siwarats.dinosaurjump.game;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import java.util.Calendar;

/**
 * Created by siwarats on 30/1/2560.
 */

public class Obstacle implements ValueAnimator.AnimatorUpdateListener, Animator.AnimatorListener {

    private View mObstacle, mCharacter;

    private ViewGroup mParent;

    private AnimatorSet animatorSet;

    private GameOverListener mGameOverListener;

    public Obstacle(ViewGroup parent, View target, View character) {
        this.mParent = parent;
        this.mObstacle = target;
        this.mCharacter = character;
        init();
    }

    private void init() {
        int start = mParent.getWidth();
        int end = -100;
        ObjectAnimator tranXAnimation = ObjectAnimator.ofFloat(mObstacle,
                "" + Calendar.getInstance().getTimeInMillis(),
                start, end);
        long duration = (long) (Math.abs(end - start) * 1.5);
        tranXAnimation.addUpdateListener(this);
        tranXAnimation.setDuration(duration);
        tranXAnimation.setInterpolator(new LinearInterpolator());
        animatorSet = new AnimatorSet();
        animatorSet.setDuration(duration);
        animatorSet.addListener(this);
        animatorSet.play(tranXAnimation);
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        float x = (float) animation.getAnimatedValue();
        getTarget().setTranslationX(x);
        if (isCharacterBoundObstacles()) {
            cancel();
            if (mGameOverListener != null) {
                mGameOverListener.onGameOver(this);
            }
        }
    }

    public void start() {
        mParent.addView(mObstacle);
        animatorSet.start();
    }

    public void cancel() {
        animatorSet.cancel();
    }

    public View getTarget() {
        return mObstacle;
    }

    private boolean isCharacterBoundObstacles() {

        Rect obstacle = new Rect();
        Rect character = new Rect();

        mObstacle.getHitRect(obstacle);
        mCharacter.getHitRect(character);

        obstacle.set(obstacle.left, (obstacle.top + obstacle.bottom) /2,
                obstacle.right, obstacle.bottom);

        return Rect.intersects(obstacle, character);

    }

    public void setGameOverListener(GameOverListener gameOverListener) {
        this.mGameOverListener = gameOverListener;
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        mParent.removeView(mObstacle);
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    public interface GameOverListener {
        void onGameOver(Obstacle obstacle);
    }
}
