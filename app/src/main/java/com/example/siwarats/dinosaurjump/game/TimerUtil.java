package com.example.siwarats.dinosaurjump.game;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by siwarat.s on 16/6/2559.
 */
public class TimerUtil {

    private long tempCountDown, countDown, timeTravel;

    private ScheduledFuture<?> schedule;
    private ScheduledExecutorService scheduledExecutorService;

    private OnTimeChange onTimeChange = new OnTimeChange() {
        @Override
        public void onTravel(long milli) {

        }

        @Override
        public void onFinish() {

        }
    };

    public TimerUtil(long milliCountDown, long milliTimeTravel, OnTimeChange onTimeChange) {
        tempCountDown = milliCountDown;
        timeTravel = milliTimeTravel;
        if (onTimeChange != null) {
            this.onTimeChange = onTimeChange;
        }
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
    }

    public void start() {
        cancel();
        countDown = tempCountDown;
        schedule = scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (countDown > 0) {
                    onTimeChange.onTravel(countDown);
                    countDown -= timeTravel;
                } else {
                    schedule.cancel(true);
                    onTimeChange.onFinish();
                }
            }
        }, timeTravel, timeTravel, TimeUnit.MILLISECONDS);
    }

    public void cancel() {
        if (schedule != null) {
            schedule.cancel(true);
        }
    }

    public interface OnTimeChange {
        void onTravel(long milli);

        void onFinish();
    }
}
