package com.example.siwarats.dinosaurjump.game;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.siwarats.dinosaurjump.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_dinosaur_jump_game)
public class DinosaurJumpGameFragment extends Fragment implements
        DinosaurJumpGameContract.View {

    @ViewById
    protected ImageView imgCharacter;
    @ViewById
    protected FrameLayout backgroundContent;
    @ViewById
    protected TextView tvScore;

    @Bean
    protected DinosaurJumpGamePresenter presenter;

    private DinosaurJumpGameContract.Communicator communicator;

    @AfterViews
    protected void init() {
        presenter.init(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    public void setTextViewScore(final String text) {
        tvScore.post(new Runnable() {
            @Override
            public void run() {
                tvScore.setText(text);
            }
        });
    }

    @Override
    public void showEndGameDialog(long score, DialogInterface.OnClickListener positive) {
        new AlertDialog.Builder(getContext())
                .setCancelable(false)
                .setTitle("You lose")
                .setMessage("Your score: " + score)
                .setPositiveButton("Restart", positive)
                .show();
    }

    @Override
    public void showPauseDialog(DialogInterface.OnClickListener positive) {
        new AlertDialog.Builder(getContext())
                .setCancelable(false)
                .setTitle("Pause")
                .setMessage("Game is paused")
                .setPositiveButton("Resume", positive)
                .show();
    }

    @Override
    public void showStartDialog(DialogInterface.OnClickListener positive) {
        new AlertDialog.Builder(getContext())
                .setCancelable(false)
                .setTitle("WTF game")
                .setMessage("Are you ready?")
                .setPositiveButton("Start now", positive)
                .show();
    }

    @Override
    public ImageView getCharacter() {
        return imgCharacter;
    }

    @Override
    public FrameLayout getObstacleContainer() {
        return backgroundContent;
    }

    @Touch({R.id.rootLayout, R.id.imgCharacter})
    protected void onJumpClick(MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_DOWN) {
            presenter.onJumpClick();
        }
    }

    @Override
    public DinosaurJumpGameContract.Communicator getCommunicator() {
        return communicator;
    }

    public void setCommunicator(DinosaurJumpGameContract.Communicator communicator) {
        this.communicator = communicator;
    }
}

