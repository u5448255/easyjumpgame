package com.example.siwarats.dinosaurjump;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.siwarats.dinosaurjump.game.DinosaurJumpGameFragment;
import com.example.siwarats.dinosaurjump.game.DinosaurJumpGameFragment_;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String fragmentTag = DinosaurJumpGameFragment.class.getName();
                DinosaurJumpGameFragment fragment =
                        (DinosaurJumpGameFragment) getSupportFragmentManager()
                                .findFragmentByTag(fragmentTag);

                if (fragment == null) {
                    fragment = DinosaurJumpGameFragment_.builder()
                            .build();
                }

//                fragment.setCommunicator(this);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.mainContainer, fragment, fragmentTag)
                        .commit();
    }
}
