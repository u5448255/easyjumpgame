package com.example.siwarats.dinosaurjump.game;

import android.content.DialogInterface;
import android.widget.FrameLayout;
import android.widget.ImageView;

public interface DinosaurJumpGameContract {
    interface View {
        void setTextViewScore(String text);

        void showEndGameDialog(long score, DialogInterface.OnClickListener positive);

        void showPauseDialog(DialogInterface.OnClickListener positive);

        void showStartDialog(DialogInterface.OnClickListener positive);

        ImageView getCharacter();

        FrameLayout getObstacleContainer();

        Communicator getCommunicator();
    }

    interface Presenter {
        void init(View view);

        void onJumpClick();

        void onResume();

        void onPause();
    }

    interface Communicator {
    }
}
